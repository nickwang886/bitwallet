<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BinanceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Binance
Route::get('/binance', ['as' => 'binance', 'uses' => 'App\Http\Controllers\BinanceController@index']);

// Bittrex
Route::get('/bittrex', ['as' => 'bittrex', 'uses' => 'App\Http\Controllers\BittrexController@index']);

// Price
Route::get('/price/{ticker}', ['as' => 'price', 'uses' => 'App\Http\Controllers\PriceController@best']);