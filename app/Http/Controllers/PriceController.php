<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Binance;

class PriceController extends Controller
{
    //
    public function best(Request $request, $ticker)
    {    	
    	// Get binance
    	$api = new Binance\API("<testnet api key>","<testnet secret>", true);    	

    	$binanceData = $api->prices();		  

    	if ($binanceData && array_key_exists($ticker, $binanceData)) {
    		$binancePrice = $binanceData[$ticker];
    	}

    	// Bittrex
    	$bittrexData = Http::get('https://api.bittrex.com/v3/markets/tickers');      	

    	if ($bittrexData->json()) {    	
    		$arrSymbol = array_column($bittrexData->json(), 'symbol');

		    if (in_array($ticker, $arrSymbol)) {
		    	foreach ($bittrexData->json() as $key => $value) {
		    		if ($ticker == $value['symbol']) {
		    			$bittrexPrice = $value['bidRate'];	
		    		}
		    	}		    	
		    }					
    	}

    	// Compare
    	if (isset($binancePrice) && isset($bittrexPrice)) {
    		if ($biancePrice < $bittrexPrice) {
    			$vendor = 'binance';
    			$bestPrice = $binancePrice;
    		} else {
    			$vendor = 'bittrex';
    			$bestPrice = $bittrexPrice;
    		}
    	} else if (isset($binancePrice)) {
    		$vendor = 'binance';
    		$bestPrice = $binancePrice;
    	} else if (isset($bittrexPrice)) {
    		$vendor = 'bittrex';
    		$bestPrice = $bittrexPrice;
    	} else {
    		$vendor = 'n.a.';
    		$bestPrice = 'n.a.';
    	}

		return view('price', ['vendor' => $vendor, 'price' => $bestPrice]);
    }
}
