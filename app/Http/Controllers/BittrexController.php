<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BittrexController extends Controller
{
    //
    public function index()
    {
    	$ticker = Http::get('https://api.bittrex.com/v3/markets/tickers');    	    	

		return view('bittrex', ['tickers' => $ticker->json()]);
    }
}
