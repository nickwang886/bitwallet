<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Binance;

class BinanceController extends Controller
{
    //
    public function index()
    {
    	$api = new Binance\API("<testnet api key>","<testnet secret>", true);    	

    	$ticker = $api->prices();		

		return view('binance', ['tickers' => $ticker]);
    }
}
